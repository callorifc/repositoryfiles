package br.com.cge.repositoryFiles.model;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name="orgaos")
@Entity
public class Orgao implements EntidadeBase{
	@Id
	@Column(name="id_orgao", nullable=false, unique=true)
	private int idOrgao;
	
	@Column(name = "sigla",nullable = false)
    private String sigla;
	
	@Column(name="descricao_orgao")
    private String descricaoOrgao;
    
    
 // construtor padrão
    public Orgao() {
    }
 
    // construtor alternativo
    public Orgao(int id, String sigla, String descricao) {
        this.idOrgao = id;
        this.sigla = sigla;
        this.descricaoOrgao = descricao;
    }
    
    
	public int getIdOrgao() {
		return idOrgao;
	}
	public void setIdOrgao(int idOrgao) {
		this.idOrgao = idOrgao;
	}
	public String getSigla() {
		return sigla;
	}
	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
	public String getDescricaoOrgao() {
		return descricaoOrgao;
	}
	public void setDescricaoOrgao(String descricaoOrgao) {
		this.descricaoOrgao = descricaoOrgao;
	}

	@Override
	public int hashCode() {
		return Objects.hash(idOrgao);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Orgao other = (Orgao) obj;
		return idOrgao == other.idOrgao;
	}
	
	@Override
	public String toString() {
		return String.valueOf(this.idOrgao);
	}

	@Override
	public Integer getId() {
		// TODO Auto-generated method stub
		return this.getIdOrgao();
	}
}