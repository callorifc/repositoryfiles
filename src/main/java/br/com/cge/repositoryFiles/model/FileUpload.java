package br.com.cge.repositoryFiles.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name="file_uploads")
@Entity
public class FileUpload implements EntidadeBase{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_file", nullable=false, unique=true)
	private Integer idFile;
	
	@Column(name="path",nullable=false)
	private String path;
	
	@Column(name="senha_acesso")
	private String senhaAcesso;
	
	@Column(name="descricao")
	private String descricao;
	
	@Column(name="tipo_doc")
	private String tipoDoc;
	
	@Column(name="num_processo")
	private String numProcesso;
	
	@Column(name="idUsuario") // FK
	private int idUsuario;
	
	
	// construtor padrão
    public FileUpload() {
    }
 
    // construtor alternativo
    public FileUpload(int id, String path, String senha, String descricao, String tipoDoc, String numProcesso, int idUsuario) {
        this.idFile = id;
        this.path = path;
        this.senhaAcesso = senha;
        this.descricao = descricao;
        this.tipoDoc = tipoDoc;
        this.numProcesso = numProcesso;
        this.idUsuario = idUsuario;
    }
	
	
	public Integer getIdFile() {
		return idFile;
	}

	public void setIdFile(Integer idFile) {
		this.idFile = idFile;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getSenhaAcesso() {
		return senhaAcesso;
	}

	public void setSenhaAcesso(String senhaAcesso) {
		this.senhaAcesso = senhaAcesso;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getTipoDoc() {
		return tipoDoc;
	}

	public void setTipoDoc(String tipoDoc) {
		this.tipoDoc = tipoDoc;
	}

	public String getNumProcesso() {
		return numProcesso;
	}

	public void setNumProcesso(String numProcesso) {
		this.numProcesso = numProcesso;
	}

	public int getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}

	@Override
	public Integer getId() {
		// TODO Auto-generated method stub
		return this.getIdFile();
	}
}