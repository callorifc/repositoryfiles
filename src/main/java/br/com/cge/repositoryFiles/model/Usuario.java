package br.com.cge.repositoryFiles.model;

import java.io.Serializable;

import javax.faces.bean.ViewScoped;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
@ViewScoped
@Table(name="usuarios")
@Entity
public class Usuario implements Serializable,EntidadeBase{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_usuario", nullable=false, unique=true)
    private Integer idUsuario;
	
	@Column(name="matricula")
    private Integer matricula;
	
	@Column(name="CPF",nullable=false)
    private Long cpf;
	
	@Column(name="nome",nullable=false)
    private String nome;
	
	@Column(name="email_institucional",nullable=false)
    private String emailInstitucional;
	
	@Column(name="senha",nullable=false)
    private String senha;
    
	@Column(name="orgao",nullable=false)
    private String orgao;
     
    @Transient
    private String cpfMascara = "";
    
    public Usuario() {
    
    }
     
    public Integer getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}
	public Integer getMatricula() {
		return matricula;
	}
	public void setMatricula(Integer matricula) {
		this.matricula = matricula;
	}
	public Long getCpf() {
		return cpf;
	}
	public void setCpf(Long cpf) {
		this.cpf = cpf;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEmailInstitucional() {
		return emailInstitucional;
	}
	public void setEmailInstitucional(String emailInstitucional) {
		this.emailInstitucional = emailInstitucional;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	
	public String getOrgao() {
		return orgao;
	}
	public void setOrgao(String orgao) {
		this.orgao = orgao;
	}

	public String getCpfMascara() {
		return cpfMascara;
	}

	public void setCpfMascara(String cpfMascara) {
		this.cpfMascara = cpfMascara;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idUsuario == null) ? 0 : idUsuario.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Usuario other = (Usuario) obj;
		if (idUsuario == null) {
			if (other.idUsuario != null)
				return false;
		} else if (!idUsuario.equals(other.idUsuario))
			return false;
		return true;
	}

	@Override
	public Integer getId() {
		// TODO Auto-generated method stub
		return this.getIdUsuario();
	}
}