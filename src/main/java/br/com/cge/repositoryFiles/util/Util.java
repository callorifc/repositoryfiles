package br.com.cge.repositoryFiles.util;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import br.com.cge.repositoryFiles.model.Usuario;

public class Util {
	public static HttpSession getSession() {
		return (HttpSession)
				FacesContext.
				getCurrentInstance().
				getExternalContext().
				getSession(false);
	}

	public static HttpServletRequest getRequest() {
		return (HttpServletRequest) FacesContext.
				getCurrentInstance().
				getExternalContext().getRequest();
	}

	public static String getUserName()
	{
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		return  session.getAttribute("username").toString();
	}
	
	public static Usuario getUsuarioLogado()
	{
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		return  (Usuario)session.getAttribute("usuarioLogado");
	}

	public static String getUserId()
	{
		HttpSession session = getSession();
		if ( session != null )
			return (String) session.getAttribute("userid");
		else
			return null;
	}
	
	public static String getNewHash(){
		String hash = "";
		do {
			int random;
			boolean v = false;
			
			for(int i = 0; i<12; i++) {
				v = false;
			
				do {
					random = (int) (Math.random() * 122);
					
					if ((random >= 48 && random <= 57) ||  (random >= 65 && random <= 90) || (random >= 97 && random <= 122) ) {
						v = true;
					}
				} while (!v);
				hash += ((char)random);
			}
		}while(!valida(hash));
		
		return hash;
	}

	private static boolean valida(String hash) {
		// VALIDAR HASH SEAP
		return true;
	}
}
