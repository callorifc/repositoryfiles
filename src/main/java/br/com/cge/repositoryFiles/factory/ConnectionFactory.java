package br.com.cge.repositoryFiles.factory;

import java.sql.Connection;
import java.sql.DriverManager;



public class ConnectionFactory {
	
	private static final String USERNAME = "root";
	private static final String PASSWORD = "123456";
	private static final String DATABASE_URL = "jdbc:mariadb://172.16.120.100:3306/repository_files";
	
	public static Connection createConnection() throws Exception {
		Class.forName("org.mariadb.jdbc.Driver");
		
		Connection connection = DriverManager.getConnection(DATABASE_URL, USERNAME, PASSWORD);
		
		return connection;
	}
	
	public static Connection createOracleConnection() throws Exception{
		Class.forName("oracle.jdbc.driver.OracleDriver");
        Connection conexao = DriverManager.getConnection("jdbc:oracle:thin:@10.43.14.74:1521/srv_prod_seap", "wanderlima_age", "wedfeF#");

        return conexao;
	}
	
	public static void main(String[] args) throws Exception {
		Connection con = createOracleConnection();
		
		if(con!=null) {
			System.out.println("Conexão Obtida com Sucesso!");
			con.close();
		}
	}
}


