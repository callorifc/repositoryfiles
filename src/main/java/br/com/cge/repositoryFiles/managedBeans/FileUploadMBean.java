package br.com.cge.repositoryFiles.managedBeans;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.sql.SQLException;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfWriter;

import br.com.cge.repositoryFiles.dao.FileUploadDAO;
import br.com.cge.repositoryFiles.model.FileUpload;
import br.com.cge.repositoryFiles.util.Util;

@ManagedBean(name="UploadMB")
@RequestScoped
public class FileUploadMBean {
	private FileUpload fileupload = new FileUpload();
	private String dir;
	private String message;
	private StreamedContent file; 
	private String tipoDoc;
	
    public StreamedContent getFile() {
		return file;
	}

	public void setFile(StreamedContent file) {
		this.file = file;
	}

	public FileUpload getFileupload() {
		return fileupload;
	}

	public void setFileupload(FileUpload fileupload) {
		this.fileupload = fileupload;
	}
	
	public String getDir() {
		return dir;
	}

	public void setDir(String dir) {
		this.dir = dir;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
		
	
	
	
	public String getTipoDoc() {
		return tipoDoc;
	}

	public void setTipoDoc(String tipoDoc) {
		this.tipoDoc = tipoDoc;
	}





	private transient UploadedFile uploadedFile; // +getter+setter

    public UploadedFile getUploadedFile() {
		return uploadedFile;
	}

	public void setUploadedFile(UploadedFile uploadedFile) {
		this.uploadedFile = uploadedFile;
	}

	public void upload() {
        
        String contentType = uploadedFile.getContentType();
        byte[] contents = uploadedFile.getContents(); // Or getInputStream()
        // ... Save it, now!
        
        
        String fileNameUploaded = uploadedFile.getFileName();
        long fileSizeUploaded = uploadedFile.getSize();
        
        
        boolean file1Success = false;
        
        
        if (uploadedFile != null && uploadedFile.getSize() > 0) {
			String fileName = uploadedFile.getFileName();

			// destination where the file will be uploaded
			File savedFile = new File("C:\\Program Files\\apache-tomcat-9.0.62\\webapps\\ROOT\\imagens", fileName);

			try (InputStream input = uploadedFile.getInputstream()) {
				Files.copy(input, savedFile.toPath());
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			
			
			String infoAboutFile = "<br/> Path gerado: <b> http://172.16.121.143:8080/imagens/"+fileName+"</b>";      //http://localhost:8282/imagens/"+fileName+"
	             
	            
	             
             FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                 "Sucesso! Upload realizado!", " Não deixe de Cadastrar...\n  "));
			
			// Guarda o path de armazenamento do arquivo no formato de String		
	        //setDir(savedFile.toPath().toAbsolutePath().toString());
	        setDir("http://172.16.121.143:8080/imagens/"+fileName); //"http://localhost:8282/imagens/"+fileName
	        System.out.println("Esse é o DIR: "+ getDir());
	        
	        
	        fileupload.setPath(getDir());
	        tipoDoc = uploadedFile.getContentType();
	        fileupload.setTipoDoc(tipoDoc);
	        
			file1Success = true;
		}
        if (file1Success) {	
			setMessage("File successfully uploaded"); //mensagem de sucesso no upload
		} else {
			setMessage("Error, select atleast one file!"); //mensagem de erro no upload
		}
                  
    }
	
	
	
	
    public String cadastraFile() throws SQLException {
    	System.out.println("Esse é o path salvo: "+ fileupload.getPath());
    	
    	fileupload.setSenhaAcesso(Util.getNewHash());
        fileupload.setIdUsuario(Integer.valueOf(Util.getUserId()));	
    	FileUploadDAO fileDao = new FileUploadDAO();

        if (fileDao.save(fileupload)) {
            FacesContext.getCurrentInstance().addMessage(
             null, new FacesMessage(FacesMessage.SEVERITY_INFO,
             "Sucesso!", "Arquivo cadastrado!\n  "));
            // gerarPdf();
        } else {
            FacesContext.getCurrentInstance().addMessage(
               null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Erro!",
               "ERRO no cadastro do Arquivo!"));
        }  
        return "";
	}
	    
    
    
	
	public void descarregar() throws IOException {
	    // Abaixo temos um código estático, mas
	    // obviamente você pode buscar o arquivo de onde quiser:)
		gerarPdf();
		
		//System.out.println("C:\\Download PDF\\"+fileupload.getDescricao()+".pdf"); 
		
	    InputStream in = new FileInputStream(new File("C:\\Download PDF\\"+fileupload.getDescricao()+".pdf")); //C:\\files\\images.jpg
	    file = new DefaultStreamedContent(in, "application", fileupload.getDescricao()+".pdf");
	}
	
    
	
	 public void gerarPdf() {
	        // Novo documento PDF
	        Document documentoPDF = new Document();
	        try {
	        	//cria uma instancia do documento e nomeia pdf
				PdfWriter.getInstance(documentoPDF, new FileOutputStream("C:\\Download PDF\\"+fileupload.getDescricao()+".pdf")); // Criar pasta "C:\\Download PDF"
				
				//abrindo o arquivo 
				documentoPDF.open();
				
				//definindo o tamanho da página
				documentoPDF.setPageSize(PageSize.A4);
				
				//escrevendo Título e o primeiro paragrafo
				Paragraph pTitulo =  new Paragraph(new Phrase(20F, "GERANDO UM DOCUMENTO PDF", FontFactory.getFont(FontFactory.HELVETICA, 18F)));
				pTitulo.setAlignment(Element.ALIGN_CENTER);
				documentoPDF.add(pTitulo);
				documentoPDF.add(new Paragraph("\n"+fileupload.getPath()));
			
				
			} catch (DocumentException de) {
				de.printStackTrace();
			} catch (IOException ioe) {
				ioe.printStackTrace();
			} finally {
				documentoPDF.close();
			}
	    }
    
}