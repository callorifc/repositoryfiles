package br.com.cge.repositoryFiles.managedBeans;


import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import br.com.cge.repositoryFiles.dao.OrgaoDAO;
import br.com.cge.repositoryFiles.model.Orgao;


@ManagedBean
@RequestScoped
public class OrgaoMBean {
	private Orgao orgao = new Orgao();
	
	
    public String cadastraOrgao() throws SQLException {

               OrgaoDAO orgaoDao = new OrgaoDAO();

               if (orgaoDao.insertOrgao(orgao)) {
            	   orgao = new Orgao();
                    FacesContext.getCurrentInstance().addMessage(
                     null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                     "Orgão cadastrado com sucesso!", "Sucesso!"));
               } else {
                    FacesContext.getCurrentInstance().addMessage(
                       null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Erro no cadastro do Orgão!",
                       "Erro!"));
               }
             
         return "";
    }
    
    public Orgao getOrgao() {
        return orgao;
    }

    public void setOrgao(Orgao orgao) {
        this.orgao = orgao;
    }
   
    private List<Orgao> siglas = new ArrayList<Orgao>();

	public List<Orgao> getLista() throws Exception {
		OrgaoDAO orgaodao = new OrgaoDAO();
	    this.siglas = orgaodao.listarOrgaos();

	    return siglas;
	}

	public void setLista(ArrayList<Orgao> siglas) {
	    this.siglas= siglas;
	}
   
   
}
