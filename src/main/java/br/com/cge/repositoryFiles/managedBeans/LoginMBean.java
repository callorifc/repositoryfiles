package br.com.cge.repositoryFiles.managedBeans;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import br.com.cge.repositoryFiles.dao.UsuarioDAO;
import br.com.cge.repositoryFiles.model.Usuario;

@ManagedBean(name = "loginBean")
@SessionScoped
public class LoginMBean implements Serializable{
	private Usuario login = new Usuario();
	 
	 public LoginMBean() {
		 this.login = new Usuario();
	 }
	 
	 @PostConstruct
	 public void init(){
	     this.login = new Usuario();
	 }
	  
	 public String logarNoSistema() {
		 UsuarioDAO usuarioDAO = new UsuarioDAO();
		 
		 this.login.setCpf(Long.parseLong((this.login.getCpfMascara().replace(".", "")).replace("-", "")));//retira mascara do cpf
		 
		 Usuario usuarioLogado = usuarioDAO.login(this.login);
		 
		 if(usuarioLogado != null) {
			 this.login = usuarioLogado;
			 HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
			 session.setAttribute("usuario",usuarioLogado);
			 session.setAttribute("userid", usuarioLogado.getId().toString());
			 session.setAttribute("username", usuarioLogado.getNome());
			 return "/app/index?faces=redirect=true";
		 }else {
			 limparLogin(this.login);
			 return "/seguranca/login?faces=redirect=true";
		 }
	 }

	private void limparLogin(Usuario login) {
		 login.setEmailInstitucional(null);
		 login.setIdUsuario(null);
		 login.setMatricula(null);
		 login.setNome(null);
		 login.setOrgao(null);
	}

	 public String logout() {
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		return "/seguranca/login?faces=redirect=true";
	 }
	 
	public Usuario getLogin() {
		return this.login;
	}

	public void setLogin(Usuario login) {
		this.login = login;
	}
}
