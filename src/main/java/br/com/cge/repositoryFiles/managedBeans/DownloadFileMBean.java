package br.com.cge.repositoryFiles.managedBeans;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import br.com.cge.repositoryFiles.dao.FileUploadDAO;
import br.com.cge.repositoryFiles.model.FileUpload;

@SessionScoped
@ManagedBean(name="DownloadMB")
public class DownloadFileMBean {
		
	private StreamedContent file;

	public StreamedContent getFile() {
		return file;
	}

	public void setFile(StreamedContent file) {
		this.file = file;
	}

	
	
	public void descarregar(String pathsave) throws IOException {
	    		
		System.out.println("Path do File no servidor"+pathsave);
		String [] diretorioFile = pathsave.split("/");
		System.out.println("\n**** Name file: "+diretorioFile[diretorioFile.length - 1]);
		

	    InputStream in = new FileInputStream(new File("C:\\Program Files\\apache-tomcat-9.0.62\\webapps\\ROOT\\imagens\\"+diretorioFile[diretorioFile.length - 1])); //C:\\files\\images.jpg
	    file = new DefaultStreamedContent(in, "application", "FileDownload_chavealeatoria_"+diretorioFile[diretorioFile.length - 1]);
	}
	
	


	private ArrayList<FileUpload> links = new ArrayList<FileUpload>();

	public ArrayList<FileUpload> getLista() throws Exception {
		FileUploadDAO fileupDao = new FileUploadDAO();
	    this.links = fileupDao.listarPaths();
	   
	    /*for(Orgao e : this.siglas){
	                
	         String sigla = e.getSigla();      
	         System.out.println(sigla);
	         
	    }*/
	    
	    return links;
	    
	}

	public ArrayList<FileUpload> getLinks() {
		return links;
	}

	public void setLinks(ArrayList<FileUpload> links) {
		this.links = links;
	}


}
