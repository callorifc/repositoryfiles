package br.com.cge.repositoryFiles.managedBeans;

import java.sql.SQLException;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import br.com.cge.repositoryFiles.dao.OrgaoDAO;
import br.com.cge.repositoryFiles.dao.UsuarioDAO;
import br.com.cge.repositoryFiles.model.Orgao;
import br.com.cge.repositoryFiles.model.Usuario;
import br.com.cge.repositoryFiles.util.Util;

@ManagedBean
@ViewScoped
public class UsuarioMBean {
	private Usuario usuario = new Usuario();

    public String cadastraUsuario() throws SQLException {

               UsuarioDAO usuarioDao = new UsuarioDAO();

               if (usuarioDao.insertUsuario(usuario)) {
                    FacesContext.getCurrentInstance().addMessage(
                     null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                     "Sucesso!", "Usuário cadastrado com sucesso!"));
               } else {
                    FacesContext.getCurrentInstance().addMessage(
                       null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Erro!",
                       "Erro no cadastro de usuário!"));
               }
         return "";
    }
    
    
    public void pesquisarUsuarioSEAP() {
    	UsuarioDAO usuarioDAO = new UsuarioDAO();
    	Usuario userTemp = usuarioDAO.pesquisarUsuarioSEAP(this.usuario.getCpf());
    	if(userTemp == null) {
    		 FacesContext.getCurrentInstance().addMessage(
                     null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Erro!",
                     "Usuário não encontrado na base SEAP. Cadastro não permitido!"));
    		 return;
    	}
    	this.usuario.setCpf(userTemp.getCpf());
    	this.usuario.setEmailInstitucional(userTemp.getEmailInstitucional());
    	this.usuario.setMatricula(userTemp.getMatricula());
    	this.usuario.setNome(userTemp.getNome());
    	this.usuario.setOrgao(userTemp.getOrgao());
    	this.usuario.setSenha(userTemp.getSenha());
    }
    
   public Usuario getUsuario() {
        return usuario;
   }

   public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
   }
   
   public List<Orgao> getLista() throws Exception {
		OrgaoDAO orgaodao = new OrgaoDAO();
	    return orgaodao.listarOrgaos();
	}
}
