package br.com.cge.repositoryFiles.converter;

import java.io.Serializable;

import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import br.com.cge.repositoryFiles.dao.OrgaoDAO;
import br.com.cge.repositoryFiles.model.Orgao;
@ViewScoped
@FacesConverter(value = "orgaoConverter")
public class OrgaoConverter implements Converter, Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public OrgaoConverter() {
	        
	}
	
	
	
	@Override
    public Object getAsObject(FacesContext ctx, UIComponent component, String value) {
		System.out.println("** Converter OBJ. ");
        if (value != null) {
            int id = Integer.parseInt(value);
            
            OrgaoDAO orgaoDao = new OrgaoDAO();
            try {
				Orgao orgao = orgaoDao.buscarPorID(id);
				return orgao;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            
        }
        return null;
    }
 
    @Override
    public String getAsString(FacesContext ctx, UIComponent component, Object obj) {
    	System.out.println("** Converter STRING. ");
    	try {
    		if (obj != null && !"".equals(obj)) {
	        	OrgaoDAO orgaoDao = new OrgaoDAO();
	        	Orgao orgao = orgaoDao.buscarPorID(Integer.parseInt(obj.toString()));
	        	if(orgao !=null) {
	        		Integer codigo = orgao.getIdOrgao(); 
					if (codigo != null) { 
						return String.valueOf(codigo); 
					}
	        	}
    		}
    	}catch(Exception e) {
    		e.printStackTrace();
    	}
        return "";  
   }
}