package br.com.cge.repositoryFiles.converter;

import java.io.Serializable;
import java.util.Map;

import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import br.com.cge.repositoryFiles.model.Orgao;

@ViewScoped
@FacesConverter(value = "usuarioConverter")
public class UsuarioConverter implements Converter, Serializable {
		
		/**
		 **/
		private static final long serialVersionUID = 1L;
		
		public UsuarioConverter() {
		        
		}

		@Override
	    public Object getAsObject(FacesContext ctx, UIComponent component, String value) {
	        if (value != null) {
	            return this.getAttributesFrom(component).get(value);
	        }
	        return null;
	    }
	 
	    @Override
	    public String getAsString(FacesContext ctx, UIComponent component, Object value) {
	        if (value != null && !"".equals(value)) {
	            Orgao orgao = (Orgao) value;
	            this.addAttribute(component, orgao);
	            Integer codigo = orgao.getIdOrgao();
	            if (codigo != null) {
	                return String.valueOf(codigo);
	            }
	        }
	        return (String) value;
	    }
	    
	    
	    
	    
	    protected void addAttribute(UIComponent component, Orgao o) {
	    	 String key = String.valueOf(o.getIdOrgao());   
	        this.getAttributesFrom(component).put(key, o);
	    }
	 
	    protected Map<String, Object> getAttributesFrom(UIComponent component) {
	        return component.getAttributes();
	    }
		

}
