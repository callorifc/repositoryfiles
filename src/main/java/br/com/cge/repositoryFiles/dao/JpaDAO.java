package br.com.cge.repositoryFiles.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import br.com.cge.repositoryFiles.model.EntidadeBase;

public class JpaDAO<E extends EntidadeBase> {
	private static EntityManagerFactory entityManagerFactory;
	
	static {
		entityManagerFactory = Persistence.createEntityManagerFactory("Clientes-PU");
	}
	
	public JpaDAO() {
	}
	
	/**
	 * BUSCA OBJETO NA BASE POR ID
	 * @param clazz
	 * @param id
	 * @return
	 */
	public E buscarPorID(Class<E> clazz,int id) {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		
		E entidade = entityManager.find(clazz, id);
		entityManager.close();
		
		return entidade;
	}
	
	
	/**
	 * BUSCA OBJETO NA BASE POR CPF
	 * @param clazz
	 * @param cpf
	 * @return
	 */
	/*
	 * public E buscarPorCpf(Class<E> clazz,String cpf) { EntityManager
	 * entityManager = entityManagerFactory.createEntityManager();
	 * 
	 * E entidade = entityManager.find(clazz, cpf); entityManager.close();
	 * 
	 * return entidade; }
	 */
	
	
	/**
	 * ADICIONA OU ATUALIZA OBJETO NA BASE
	 * @param obj
	 */
	public void SalvarOuEditar(E obj){
		EntityManager manager = entityManagerFactory.createEntityManager();
		try{
			manager.getTransaction().begin();
			if(obj.getId() == null){
				manager.persist(obj);
			}else{
				manager.merge(obj);
			}
			manager.getTransaction().commit();
		}catch(Exception e){
			e.printStackTrace();
			manager.getTransaction().rollback();
		}
	}
	
	/**
	 * DELETA OBJETO DA BASE
	 * @param clazz
	 * @param id
	 */
	public void excluir(Class<E> clazz, Integer id){
		EntityManager manager = entityManagerFactory.createEntityManager();
		E t = buscarPorID(clazz, id);
		try{
			manager.getTransaction().begin();
			manager.remove(t);
			manager.getTransaction().commit();
		}catch (Exception e) {
			e.printStackTrace();
			manager.getTransaction().rollback();
		}
	}
	
	/**
	 * LISTA TODOS OS OBJETOS PARA UM TIPO DE CLASSE ESPECIFICO E PODE ORDENAR ASCENDENTE OU DECRESCENTE
	 * @param clazz
	 * @param ordenacao
	 * @param propertiesOrder
	 * @return
	 */
	public List<E> listarTodos(Class<E> clazz,String ordenacao,String ...propertiesOrder) {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
	    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
	    CriteriaQuery<E> cq = cb.createQuery(clazz);
	    Root<E> root = cq.from(clazz);
	         
	    List<javax.persistence.criteria.Order> orders = new ArrayList<>();
	    for (String propertyOrder : propertiesOrder) {
	        if (ordenacao.equals("DESC")) {
	            orders.add(cb.desc(root.get(propertyOrder)));
	        } else {
	            orders.add(cb.asc(root.get(propertyOrder)));
	        }
	    }
	    cq.orderBy(orders);
	 
	    return entityManager.createQuery(cq).getResultList();
	}

	public static EntityManagerFactory getEntityManagerFactory() {
		return entityManagerFactory;
	}

	public static void setEntityManagerFactory(EntityManagerFactory entityManagerFactory) {
		JpaDAO.entityManagerFactory = entityManagerFactory;
	}
	
	
	
}