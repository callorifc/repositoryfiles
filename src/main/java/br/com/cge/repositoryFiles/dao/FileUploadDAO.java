package br.com.cge.repositoryFiles.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import br.com.cge.repositoryFiles.factory.ConnectionFactory;
import br.com.cge.repositoryFiles.model.FileUpload;

public class FileUploadDAO {
	public boolean save (FileUpload fileupload) {
		String sql = "INSERT INTO file_uploads(path, senha_acesso, descricao, tipo_doc, num_processo, id_usuario) VALUES (?,?,?,?,?,?)";
		
		Connection conn = null;
		PreparedStatement pstm = null;
		try {
			conn = ConnectionFactory.createConnection();
			pstm = (PreparedStatement) conn.prepareStatement(sql);
			//pstm.setInt(1, fileupload.getId_file());
			pstm.setString(1, fileupload.getPath());
			pstm.setString(2, fileupload.getSenhaAcesso());
			pstm.setString(3, fileupload.getDescricao());
			pstm.setString(4, fileupload.getTipoDoc());
			pstm.setString(5, fileupload.getNumProcesso());
			pstm.setInt(6, fileupload.getIdUsuario());
			
			pstm.execute();
			return true;
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}finally {
			try {
				if(pstm!=null) {
					pstm.close();
				}
				
				if (conn!=null) {
					conn.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}	
	}
	
	
	
	
	// método que permite obter a lista de path
		public ArrayList<FileUpload> listarPaths() throws Exception {
	        ArrayList<FileUpload> listaPaths = new ArrayList<FileUpload>();
	       
	        Connection conn = ConnectionFactory.createConnection();
			
	        try {
						
				if(conn!=null){
					String sql = "SELECT * FROM file_uploads";
					
					Statement stmt = conn.createStatement();
					ResultSet rs = stmt.executeQuery(sql);
					
					while(rs.next()) {
						int id = rs.getInt("id_file");
						String path = rs.getString("path");
						String senha = rs.getString("senha_acesso");
						String descricao = rs.getString("descricao");
						String tipo_doc = rs.getString("tipo_doc");
						String num_processo = rs.getString("num_processo");
						int id_usuario = rs.getInt("id_usuario");
						FileUpload fileup = new FileUpload(id, path, senha, descricao, tipo_doc, num_processo, id_usuario);

						listaPaths.add(fileup);
						
						//System.out.println("Paths: " + listaPaths);
					}
					System.out.println("query record in table success. ");
					}
		       } catch (SQLException ex) {
					ex.printStackTrace();
				}finally {
					try {
						if (conn!=null) {
							conn.close();
						}
					}catch (Exception e) {
						e.printStackTrace();
					}
				}
		
		       return listaPaths;
		}
	
	
	
	
}
