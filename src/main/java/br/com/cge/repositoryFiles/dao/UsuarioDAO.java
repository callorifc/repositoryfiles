package br.com.cge.repositoryFiles.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import br.com.cge.repositoryFiles.factory.ConnectionFactory;
import br.com.cge.repositoryFiles.model.Usuario;
import br.com.cge.repositoryFiles.util.Util;
import org.apache.commons.lang3.StringUtils;

public class UsuarioDAO {
	
	public boolean insertUsuario(Usuario usuario) {
		String sql = "INSERT INTO usuarios (matricula, CPF, nome, email_institucional, senha, orgao) VALUES (?,?,?,?,?,?)";
		
		Connection conn = null;
		PreparedStatement pstm = null;
		
		try {
			conn = ConnectionFactory.createConnection();
			
			pstm = (PreparedStatement) conn.prepareStatement(sql);		
			//pstm.setInt(1, usuario.getId_usuario());
			pstm.setInt(1, usuario.getMatricula());
			pstm.setLong(2, usuario.getCpf());
			pstm.setString(3, usuario.getNome());
			pstm.setString(4, usuario.getEmailInstitucional());
			pstm.setString(5, usuario.getSenha());
			pstm.setString(6, usuario.getOrgao());
	
			pstm.execute();
			return true;
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}finally {
			try {
				if(pstm!=null) {
					pstm.close();
				}
				if (conn!=null) {
					conn.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}	
	}
	
	
	public Usuario login(Usuario login) {
		if(login ==null || login.getCpf()==null || login.getSenha()==null || login.getSenha().equals("")) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,"LoginDAO!",
	    			 "Usuário não encontrado, verifique o login e senha informados!"));
			return null;
		}
		
		Usuario usuarioLogado = null;
		Connection conn = null;
		String sql = "SELECT id_usuario,matricula, CPF, nome, email_institucional, senha, orgao FROM usuarios WHERE CPF= ? and senha= ?";
		PreparedStatement pstm = null;
		
	  try {
		 conn = ConnectionFactory.createConnection();
	     pstm = (PreparedStatement) conn.prepareStatement(sql);
	     pstm.setLong(1, login.getCpf());
	     pstm.setString(2, login.getSenha());
	     ResultSet rs = pstm.executeQuery();
	     if(rs.next()) {
	    	 usuarioLogado = new Usuario();
	    	 usuarioLogado.setIdUsuario(rs.getInt("id_usuario"));
	    	 usuarioLogado.setMatricula(rs.getInt("matricula"));
	    	 usuarioLogado.setCpf(rs.getLong("CPF"));
	    	 usuarioLogado.setNome(rs.getString("nome"));
	    	 usuarioLogado.setEmailInstitucional(rs.getString("email_institucional"));
	    	 usuarioLogado.setSenha(rs.getString("senha"));
	    	 usuarioLogado.setOrgao(rs.getString("orgao"));
	     }else {
	    	 FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,"LoginDAO!",
	    			 "Usuário não encontrado, verifique o login e senha informados!"));
	    	 usuarioLogado= null;
	     }
	  } catch (Exception ex) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
					"Erro de conexão na Base","Impossível conectar na base de dados"));   
			ex.printStackTrace();
			usuarioLogado=null;
	 } finally {
			try {
				if(pstm!=null) {
					pstm.close();
				}
				if (conn!=null) {
					conn.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
	  return usuarioLogado;
	}


	public Usuario pesquisarUsuarioSEAP(Long cpf) {
	String sql = "SELECT  \r\n"
			+ "  MAX(ORG.NOMESETOR) AS ORGAO,\r\n"
			+ "  FUNC.NUMERO AS MATRICULA,\r\n"
			+ "  FUNC.NOME AS NOME_SERVIDOR,\r\n"
			+ "  FUNC.CPF,\r\n"
			+ "  FUNC.E_MAIL\r\n"
			+ " \r\n"
			+ "    \r\n"
			+ "from  ERGON.FITABANCO FB\r\n"
			+ "      JOIN ERGON.FUNCIONARIOS FUNC on FB.NUMFUNC = FUNC.NUMERO\r\n"
			+ "      join ERGON.VINCULOS VINC on FB.numvinc = VINC.NUMERO AND FUNC.NUMERO = VINC.NUMFUNC\r\n"
			+ "      join HADES.HSETOR_ ST ON ST.SETOR = FB.SETOR AND ST.DATAFIM IS NULL\r\n"
			+ "      join HADES.HSETOR_ ORG ON ORG.SUBEMP_CODIGO = ST.SUBEMP_CODIGO AND ORG.DATAFIM IS NULL AND nvl(ORG.PAISETOR,0) = 0 and org.tiposetor = 'ORGAO'\r\n"
			+ "      left join ERGON.CARGOS_ CARGO  on CARGO.CARGO = FB.CARGO\r\n"
			+ "  \r\n"
			+ "WHERE func.cpf= ? \r\n"
			+ "      and FB.MES_ANO= ?  \r\n"
			+ "   \r\n"
			+ "GROUP BY    FUNC.NUMERO,\r\n"
			+ "            FUNC.NOME,\r\n"
			+ "            FUNC.CPF,\r\n"
			+ "            FUNC.E_MAIL";
		
		Connection conn = null;
		PreparedStatement pstm = null;
		
		try {
			conn = ConnectionFactory.createOracleConnection();
			
			pstm = (PreparedStatement) conn.prepareStatement(sql);		
			pstm.setLong(1, cpf);
			pstm.setString(2, "01/05/2022");
	
			ResultSet rs = pstm.executeQuery();
			
			Usuario userTemp = new Usuario();
			if(rs.next()) {
				userTemp.setCpf(rs.getLong("CPF"));
				userTemp.setEmailInstitucional(rs.getString("E_MAIL"));
				userTemp.setMatricula(rs.getInt("MATRICULA"));
				userTemp.setNome(rs.getString("NOME_SERVIDOR"));
				userTemp.setOrgao(rs.getString("ORGAO"));
				userTemp.setSenha(StringUtils.leftPad(String.valueOf(rs.getLong("CPF")),11,'0'));
			} else {
				return null;
			}
			
			return userTemp;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally {
			try {
				if(pstm!=null) {
					pstm.close();
				}
				if (conn!=null) {
					conn.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}	
	}
}