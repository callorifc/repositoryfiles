package br.com.cge.repositoryFiles.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import br.com.cge.repositoryFiles.factory.ConnectionFactory;

public class LoginDAO {
	public static boolean login(String user, String password) {
		Connection con = null;
		PreparedStatement ps = null;
		
		try {
			con = ConnectionFactory.createConnection();
			ps = con.prepareStatement(
					"SELECT email_institucional, senha FROM usuarios WHERE email_institucional= ? and senha= ? ");
			ps.setString(1, user);
			ps.setString(2, password);

			ResultSet rs = ps.executeQuery();
			if (rs.next()) // found
			{
				System.out.println(rs.getString("email_institucional"));
				return true;
			}
			else {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
						"LoginDAO!",
						"Wrong password message test!"));
				return false;
			}
		} 
		catch (Exception ex) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
					"Database Error",
					"Unable to connect database"));            
			System.out.println("Error in login() -->" + ex.getMessage());
			return false;
		} finally {
			try {
				if(ps!=null) {
					ps.close();
				}
				if (con!=null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static void main(String[] args) { 
		System.out.println(LoginDAO.login("user", "pass"));
	}
}
