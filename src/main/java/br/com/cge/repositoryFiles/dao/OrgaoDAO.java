package br.com.cge.repositoryFiles.dao;

import java.util.List;

import br.com.cge.repositoryFiles.model.Orgao;

public class OrgaoDAO extends JpaDAO<Orgao>{
	
	
	public boolean insertOrgao(Orgao orgao) {
		  try{
			  this.SalvarOuEditar(orgao);
		  }catch(Exception e) {
			  e.printStackTrace();
			  return false;
		  }
		  return true;
	}
	
	public Orgao buscarPorID(int id) {
		try{
			  return this.buscarPorID(Orgao.class, id);
		  }catch(Exception e) {
			  e.printStackTrace();
			  return null;
		  }
	}
	
	public List<Orgao> listarOrgaos() throws Exception {
         return this.listarTodos(Orgao.class,"ASC","descricaoOrgao");
	}
}