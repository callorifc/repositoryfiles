package br.com.cge.repositoryFiles.filtros;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import br.com.cge.repositoryFiles.model.Usuario;

/**
 * Servlet Filter implementation class AppAuth
 */
@WebFilter(filterName = "loginAuth", urlPatterns = ("/app/*"))
public class AppAuth implements Filter {

    /**
     * Default constructor. 
     */
    public AppAuth() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		HttpSession session = (HttpSession) req.getSession();
		
		Usuario login = (Usuario) session.getAttribute("usuario");
		
		if(login == null || login.getNome()==null || login.getNome().equals("")) {
			res.sendRedirect(req.getContextPath()+"/seguranca/login.jsf");
		}else {
			chain.doFilter(request, response);
		}
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
